﻿interface IDamagable
{
    void Kill();

    void IncHealth(int amount);
    void DecHealth(int amount);

    void IncMaxHealth(int amount);
    void DecMaxHealth(int amount);
}

