﻿using UnityEngine;

public class MoveToPointHandler : MoveHandler<Vector3> {

    public Transform Point;
    public float Speed = 1.0f;

    private readonly IEntityCallback _callback;

    public MoveToPointHandler(Transform pointToMoveTo, IEntityCallback callback)
    {
        Point = pointToMoveTo;
        _callback = callback;
    }

    public override Vector3 PerformMove(GameObject entity)
    {

        var moveDirection = new Vector3();
	    var distance = Vector3.Distance(entity.transform.position, Point.position);

        if (distance > 1)
        {
            var right = entity.transform.position.z > Point.position.z ? -1 : 1;
            var up = entity.transform.position.y > Point.position.y ? -1 : 1;
            if (entity.transform.position.y == Point.position.y)
            {
                up = 0;
            }

            var zDistance = right*(entity.transform.position.z - Point.position.z);
            var yDistance = up *(entity.transform.position.y - Point.position.y);

            float[] speeds = getXYSpeed(Speed, zDistance, yDistance);
            moveDirection = new Vector3(0, speeds[1] * up,
                                    speeds[0] * right);
            moveDirection = entity.transform.TransformDirection(moveDirection);
        }
        else {
            _callback.FinishedBehavior();
        }

        var controller = (CharacterController)(entity.GetComponent("CharacterController"));

        controller.Move(moveDirection);

        return moveDirection;
    }

    private float[] getXYSpeed(float speed, float width, float height)
    {
        var speeds = new float[2];
        if (width == 0)
        {
            speeds[0] = 0;
            speeds[1] = speed;
            return speeds;
        }else if (height == 0){
            speeds[0] = speed;
            speeds[1] = 0;
            return speeds;
        }

        float radians = Mathf.Atan(height / width);
        Debug.Log("deg0eres" + radians);

        float angle = /*57.2957795f **/ radians;
        float sinAngle = Mathf.Sin(angle);
        float cosAngle = Mathf.Cos(angle);
        Debug.Log("sinangle" + sinAngle);
        Debug.Log("cosangle" + cosAngle);

        //x
        speeds[0] = speed* cosAngle/2;
        speeds[1] = speed* sinAngle/2;
        Debug.Log("speeds : " + speeds[0] + " " + speeds[1]);
        return speeds;
    }

}
