using UnityEngine;

abstract class ActionHandler<T> : Behavior<T>
{
	public override T PerformBehavior(GameObject entity){
		return PerformAction(entity);	
	}
    abstract public T PerformAction(GameObject entity);
}