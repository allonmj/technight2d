using UnityEngine;

abstract class IdleHandler<T> : Behavior<T>
{
	public override T PerformBehavior(GameObject entity){
		return PerformIdle(entity);	
	}
    abstract public T PerformIdle(GameObject entity);
}

