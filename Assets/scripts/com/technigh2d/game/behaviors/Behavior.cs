﻿using UnityEngine;
/// <summary>
/// This interface defines the standard behavior that will
/// be used by all future behaviors.
/// </summary>
public abstract class Behavior<T>
{
    /// <summary>
    /// When implemented the method should be used to define and execute
    /// a behavior. 
    /// </summary>
    public abstract T PerformBehavior(GameObject entity);
}
