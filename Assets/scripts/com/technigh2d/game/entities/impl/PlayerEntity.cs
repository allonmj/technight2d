using UnityEngine;

public class PlayerEntity : Entity, IDamagable
{

    public bool IsAlive=false;
    public int CurrentHealth=100;
    public int MaxHealth=100;

    public PlayerEntity()
    {
        IsAlive = true;
        LogInfo();
    }

 /*    void OnControllerColliderHit(ControllerColliderHit hit){
         if (hit.normal.y < 0.9)
         {
             if (hit.gameObject.tag == "enemy" && healthLock == 0)
             {
                currentHealth -= 1;
             }
             if (currentHealth > 0)
             {
                 currentHealth -= 1;
             }
             else
             {
                 isAlive = false;
                 currentHealth = 0;
             }
             logInfo();

         }
     }*/

    public void Kill()
    {
        CurrentHealth = 0;
        IsAlive = false;
    }

    public void IncHealth(int amount)
    {
        CurrentHealth += amount;
    }

    public void DecHealth(int amount)
    {
        CurrentHealth-=amount;
    }

    public void IncMaxHealth(int amount)
    {
        MaxHealth += amount;
    }

    public void DecMaxHealth(int amount)
    {
        MaxHealth -= amount;
    }

    private void LogInfo()
    {
        Debug.Log("isAlive: " + IsAlive + " currentHealth: " + CurrentHealth + " maxHealth : " + MaxHealth);
    }
}

