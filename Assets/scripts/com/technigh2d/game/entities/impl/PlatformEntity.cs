﻿using UnityEngine;
using System.Collections.Generic;

class PlatformEntity : Entity
{
    public Transform Point1;
    public Transform Point2;

    void Start()
    {
        Handler = new List<Behavior<Vector3>>();
        var b1 = new MoveToPointHandler(Point1, this) {Speed = 0.1f};
        Handler.Add(b1);
        var b2 = new MoveToPointHandler(Point2, this) {Speed = 0.1f};
        Handler.Add(b2);
    }

}

