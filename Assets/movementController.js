﻿#pragma strict
@script RequireComponent (CharacterController)

var points : Transform[];
var speed : float = 6.0;
var jumpSpeed : float = 8.0;
var gravity : float = 20.0;

private var verticalMovement : float;
private var moveDirection : Vector3 = Vector3.zero;
private var currentPoint : int = 0;

function Update() {
	var controller : CharacterController = GetComponent(CharacterController);
	
	
	if(points.Length > 0){
	if (controller.isGrounded) {
		verticalMovement = 0;
		// We are grounded, so recalculate
		// move direction directly from axes
		var z = Vector3.Distance(transform.position, points[currentPoint].position);
		if(z<=1){
			currentPoint += 1;
			if(currentPoint >= points.Length){
				currentPoint = 0;
			}
			verticalMovement = jumpSpeed;
		}
			
		
		var right = transform.position.z > points[currentPoint].position.z ? -1 :1;
		
		print(z);
		moveDirection = Vector3(0, 0,
		                        speed * right);
		moveDirection = transform.TransformDirection(moveDirection);
		//moveDirection *= speed;
		
		
	}
	
	}
	// Apply gravity
	verticalMovement -= gravity * Time.deltaTime;
	moveDirection.y = verticalMovement;
	
	// Move the controller
	
	controller.Move(moveDirection * Time.deltaTime);
}