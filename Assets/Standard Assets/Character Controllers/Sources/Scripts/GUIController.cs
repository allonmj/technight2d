﻿using UnityEngine;

public class GUIController : MonoBehaviour
{

    private bool _paused = false;

    void OnGUI(){
        if (_paused){
            if (GUI.Button(new Rect(10,50,100,30) , "Resume")){
                Time.timeScale = 1;
                _paused = false;
            }
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (!_paused)
            {
                Time.timeScale = 0;
                _paused = true;
            }
            else
            {
                Time.timeScale = 1;
                _paused = false;
            }
        }
    }
}
