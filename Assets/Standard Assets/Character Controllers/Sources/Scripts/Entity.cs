﻿using UnityEngine;
using System.Collections.Generic;

public class Entity : MonoBehaviour, IEntityCallback {

    public List<Behavior<Vector3>> Handler;
    private int _currentBehaviorPosition;

	// Use this for initialization
	void Start () {
        //override
	}
	
	// Update is called once per frame
	void Update () {
        if (Handler !=null && _currentBehaviorPosition < Handler.Count)
        {
            Handler[_currentBehaviorPosition].PerformBehavior(gameObject);
        }
	}

    public void FinishedBehavior()
    {
        if (_currentBehaviorPosition == Handler.Count-1)
        {
            _currentBehaviorPosition = 0;
        }
        else
        {
            // update is async so only increment if we can
            _currentBehaviorPosition++;
        }
    }
}
