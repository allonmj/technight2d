using UnityEngine;

public abstract class MoveHandler<T> : Behavior<T>
{
	public override T PerformBehavior(GameObject entity){
		return PerformMove(entity);	
	}

    abstract public T PerformMove(GameObject entity);
}


